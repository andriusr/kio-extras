# translation of kio_sftp.po to Latvian
# Copyright (C) 2007, 2008, 2011 Free Software Foundation, Inc.
#
# Maris Nartiss <maris.kde@gmail.com>, 2007, 2011.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008, 2009, 2010.
# Viesturs Zariņš <viesturs.zarins@mii.lu.lv>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_sftp\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-18 00:44+0000\n"
"PO-Revision-Date: 2011-12-02 07:37+0200\n"
"Last-Translator: Maris Nartiss <maris.kde@gmail.com>\n"
"Language-Team: Latvian\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#: kio_sftp.cpp:226
#, fuzzy, kde-format
#| msgid "Incorrect username or password"
msgid "Incorrect or invalid passphrase"
msgstr "Nepareizs lietotājvārds vai parole"

#: kio_sftp.cpp:276
#, kde-format
msgid "Could not allocate callbacks"
msgstr "Nebija iespējams piešķirt atpakaļizsaukumus"

#: kio_sftp.cpp:289
#, kde-format
msgid "Could not set log verbosity."
msgstr "Neizdevās iestatīt žurnalēšanas detalitāti."

#: kio_sftp.cpp:294
#, fuzzy, kde-format
#| msgid "Could not set log verbosity."
msgid "Could not set log userdata."
msgstr "Neizdevās iestatīt žurnalēšanas detalitāti."

#: kio_sftp.cpp:299
#, fuzzy, kde-format
#| msgid "Could not allocate callbacks"
msgid "Could not set log callback."
msgstr "Nebija iespējams piešķirt atpakaļizsaukumus"

#: kio_sftp.cpp:335 kio_sftp.cpp:337 kio_sftp.cpp:854
#, kde-format
msgid "SFTP Login"
msgstr "SFTP pieteikšanās"

#: kio_sftp.cpp:352
#, kde-format
msgid "Use the username input field to answer this question."
msgstr "Lietojiet lietotājvārda ievades lauku, lai atbildētu šo jautājumu."

#: kio_sftp.cpp:365
#, fuzzy, kde-format
#| msgid "Please enter your username and password."
msgid "Please enter your password."
msgstr "Lūdzu ievadiet jūsu lietotājvārdu un paroli."

#: kio_sftp.cpp:370 kio_sftp.cpp:857
#, fuzzy, kde-format
#| msgid "site:"
msgid "Site:"
msgstr "vietne:"

#: kio_sftp.cpp:417
#, fuzzy, kde-format
#| msgid "Could not read link: %1"
msgctxt "error message. %1 is a path, %2 is a numeric error code"
msgid "Could not read link: %1 [%2]"
msgstr "Neizdevās nolasīt saiti: %1"

#: kio_sftp.cpp:539
#, kde-format
msgid "Could not create a new SSH session."
msgstr "Neizdevās sākt jaunu SSH sesiju."

#: kio_sftp.cpp:550 kio_sftp.cpp:554
#, kde-format
msgid "Could not set a timeout."
msgstr "Neizdevās iestatīt noildzi."

#: kio_sftp.cpp:561
#, fuzzy, kde-format
#| msgid "Could not set port."
msgid "Could not disable Nagle's Algorithm."
msgstr "Neizdevās iestatīt portu."

#: kio_sftp.cpp:567 kio_sftp.cpp:572
#, kde-format
msgid "Could not set compression."
msgstr "Neizdevās iestatīt kompresiju."

#: kio_sftp.cpp:578
#, kde-format
msgid "Could not set host."
msgstr "Neizdevās iestatīt resursdatora nosaukumu."

#: kio_sftp.cpp:584
#, kde-format
msgid "Could not set port."
msgstr "Neizdevās iestatīt portu."

#: kio_sftp.cpp:592
#, kde-format
msgid "Could not set username."
msgstr "Neizdevās iestatīt lietotājvārdu."

#: kio_sftp.cpp:599
#, kde-format
msgid "Could not parse the config file."
msgstr "Neizdevās parsēt konfigurācijas failu."

#: kio_sftp.cpp:616
#, fuzzy, kde-kuit-format
#| msgid "Opening SFTP connection to host %1:%2"
msgid "Opening SFTP connection to host %1:%2"
msgstr "Atver SFTP savienojumu ar resursdatoru %1:%2"

#: kio_sftp.cpp:656
#, fuzzy, kde-format
#| msgid "Could not set username."
msgid "Could not get server public key type name"
msgstr "Neizdevās iestatīt lietotājvārdu."

#: kio_sftp.cpp:669
#, kde-format
msgid "Could not create hash from server public key"
msgstr ""

#: kio_sftp.cpp:678
#, kde-format
msgid "Could not create fingerprint for server public key"
msgstr ""

#: kio_sftp.cpp:738
#, fuzzy, kde-format
#| msgid ""
#| "The host key for this server was not found, but another type of key "
#| "exists.\n"
#| "An attacker might change the default server key to confuse your client "
#| "into thinking the key does not exist.\n"
#| "Please contact your system administrator.\n"
#| "%1"
msgid ""
"An %1 host key for this server was not found, but another type of key "
"exists.\n"
"An attacker might change the default server key to confuse your client into "
"thinking the key does not exist.\n"
"Please contact your system administrator.\n"
"%2"
msgstr ""
"Šī servera saimnieka atslēga netika atrasta, bet eksistē cita tipa atslēga.\n"
"iespējams ka kāds ļaundaris ir nomainījis noklusēto servera atslēgu, lai "
"iemānītu lietotājam ka tā neeksistē.\n"
"Lūdzu sazinieties ar servera administratoru.\n"
"%1"

#: kio_sftp.cpp:756
#, kde-format
msgctxt "@title:window"
msgid "Host Identity Change"
msgstr ""

#: kio_sftp.cpp:757
#, fuzzy, kde-kuit-format
#| msgid ""
#| "The host key for the server %1 has changed.\n"
#| "This could either mean that DNS SPOOFING is happening or the IP address "
#| "for the host and its host key have changed at the same time.\n"
#| "The fingerprint for the key sent by the remote host is:\n"
#| " %2\n"
#| "Please contact your system administrator.\n"
#| "%3"
msgctxt "@info"
msgid ""
"<para>The host key for the server <emphasis>%1</emphasis> has changed.</"
"para><para>This could either mean that DNS spoofing is happening or the IP "
"address for the host and its host key have changed at the same time.</"
"para><para>The %2 key fingerprint sent by the remote host is:<bcode>%3</"
"bcode>Are you sure you want to continue connecting?</para>"
msgstr ""
"Servera %1 saimnieka atslēga ir mainīta.\n"
"Tas var nozīmēt vai nu ka notiek DNS VILTOŠANA vai datora IP adrese un "
"saimnieka atslēga ir abas reizē mainītas.\n"
"Servera atslēgas pirkstu nospiedums ir:\n"
"%2\n"
"Lūdzu sazinieties ar servera administratoru.\n"
"%3"

#: kio_sftp.cpp:767
#, kde-format
msgctxt "@title:window"
msgid "Host Verification Failure"
msgstr ""

#: kio_sftp.cpp:768
#, fuzzy, kde-kuit-format
#| msgid ""
#| "The authenticity of host %1 cannot be established.\n"
#| "The key fingerprint is: %2\n"
#| "Are you sure you want to continue connecting?"
msgctxt "@info"
msgid ""
"<para>The authenticity of host <emphasis>%1</emphasis> cannot be established."
"</para><para>The %2 key fingerprint is:<bcode>%3</bcode>Are you sure you "
"want to continue connecting?</para>"
msgstr ""
"Datora %1 autentiskums nav zināms.\n"
"Atslēgas pirkstu nospiedums ir: %2\n"
"Vai tiešām vēlaties pieslēgties šim serverim?"

#: kio_sftp.cpp:777
#, kde-format
msgctxt "@action:button"
msgid "Connect Anyway"
msgstr ""

#: kio_sftp.cpp:800 kio_sftp.cpp:818 kio_sftp.cpp:833 kio_sftp.cpp:846
#: kio_sftp.cpp:898 kio_sftp.cpp:908
#, kde-format
msgid "Authentication failed."
msgstr "Neveiksmīga autentificēšanās."

#: kio_sftp.cpp:806
#, kde-format
msgid ""
"Authentication failed. The server didn't send any authentication methods"
msgstr ""

#: kio_sftp.cpp:855
#, kde-format
msgid "Please enter your username and password."
msgstr "Lūdzu ievadiet jūsu lietotājvārdu un paroli."

#: kio_sftp.cpp:866
#, kde-format
msgid "Incorrect username or password"
msgstr "Nepareizs lietotājvārds vai parole"

#: kio_sftp.cpp:915
#, kde-format
msgid ""
"Unable to request the SFTP subsystem. Make sure SFTP is enabled on the "
"server."
msgstr ""
"Neizdevās sazināties ar SFTP apakšsistēmu. Pārlicienieties ka uz servera ir "
"ieslēgts SFTP."

#: kio_sftp.cpp:920
#, kde-format
msgid "Could not initialize the SFTP session."
msgstr "Neizdevās sākt SFTP sesiju."

#: kio_sftp.cpp:924
#, kde-format
msgid "Successfully connected to %1"
msgstr "Veiksmīgi pieslēdzies %1"

#: kio_sftp.cpp:976
#, kde-format
msgid "Invalid sftp context"
msgstr ""

#: kio_sftp.cpp:1546
#, kde-format
msgid ""
"Could not change permissions for\n"
"%1"
msgstr ""
"Neizdevas izmainīt atļaujas priekš\n"
"%1"

#, fuzzy
#~| msgid ""
#~| "The host key for the server %1 has changed.\n"
#~| "This could either mean that DNS SPOOFING is happening or the IP address "
#~| "for the host and its host key have changed at the same time.\n"
#~| "The fingerprint for the key sent by the remote host is:\n"
#~| " %2\n"
#~| "Please contact your system administrator.\n"
#~| "%3"
#~ msgid ""
#~ "The host key for the server %1 has changed.\n"
#~ "This could either mean that DNS SPOOFING is happening or the IP address "
#~ "for the host and its host key have changed at the same time.\n"
#~ "The fingerprint for the %2 key sent by the remote host is:\n"
#~ "  SHA256:%3\n"
#~ "Please contact your system administrator.\n"
#~ "%4"
#~ msgstr ""
#~ "Servera %1 saimnieka atslēga ir mainīta.\n"
#~ "Tas var nozīmēt vai nu ka notiek DNS VILTOŠANA vai datora IP adrese un "
#~ "saimnieka atslēga ir abas reizē mainītas.\n"
#~ "Servera atslēgas pirkstu nospiedums ir:\n"
#~ "%2\n"
#~ "Lūdzu sazinieties ar servera administratoru.\n"
#~ "%3"

#~ msgid "Warning: Cannot verify host's identity."
#~ msgstr "Brīdinājums: nav iespējams pārbaudīt datora identitāti."

#~ msgid ""
#~ "The host key for this server was not found, but another type of key "
#~ "exists.\n"
#~ "An attacker might change the default server key to confuse your client "
#~ "into thinking the key does not exist.\n"
#~ "Please contact your system administrator.\n"
#~ "%1"
#~ msgstr ""
#~ "Šī servera saimnieka atslēga netika atrasta, bet eksistē cita tipa "
#~ "atslēga.\n"
#~ "iespējams ka kāds ļaundaris ir nomainījis noklusēto servera atslēgu, lai "
#~ "iemānītu lietotājam ka tā neeksistē.\n"
#~ "Lūdzu sazinieties ar servera administratoru.\n"
#~ "%1"

#~ msgid ""
#~ "The authenticity of host %1 cannot be established.\n"
#~ "The key fingerprint is: %2\n"
#~ "Are you sure you want to continue connecting?"
#~ msgstr ""
#~ "Datora %1 autentiskums nav zināms.\n"
#~ "Atslēgas pirkstu nospiedums ir: %2\n"
#~ "Vai tiešām vēlaties pieslēgties šim serverim?"

#~ msgid "No hostname specified."
#~ msgstr "Nav norādīts resursdatora nosaukums."
